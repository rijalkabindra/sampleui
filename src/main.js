import { createApp } from 'vue'
import App from './App.vue'
import  'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min'
import  'bootstrap/dist/js/bootstrap.bundle.js'


createApp(App).mount('#app')
